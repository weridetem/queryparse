# QueryParse #

This package provides a class with a set of methods to facilitate the writing of queries according to the basis written by IBM and some changes / additions that we consider necessary

https://www.ibm.com/support/knowledgecenter/en/SSELE6_8.0.1.3/com.ibm.isam.doc/develop/web_services_REST_API_doc.html

## Documentatio for use ##

The documentation for use is under construction

## Pagination results ##

## Overview ##
You can paginate the results of the GET collection methods by providing extra URL parameters. For example:

GET /microservice/v1/resource?items_per_page=10&page=2

## Sorting results ##

Overview
You can sort microservice results of the GET collection methods by providing extra URL parameters. For example:

GET /microservice/v1/resource?sort={"resource_status":"desc","resource_name":"asc"}

## Filtering results ##

Overview
You can filter the results of the GET collection methods by providing extra URL parameters. For example:

GET /microservice/v1/resource?filters=[{"resource_id":["gt",10],"resource_name":["contains","br"]},{"resource_status":["equals",2]}]

## Selecting fields ##

Overview
You can select the fields to be listed in the results of the GET collection methods by providing extra URL parameters. For example:

GET /microservice/v1/resource?fields=resource_id,resource_name
