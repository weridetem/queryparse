# Exemplo de utilizacao

$request = new LCP(env('METHOD_AUTORIZAR_USUARIO'), env('ENDPOINT_AUTORIZAR_USUARIO'), ['token' => $jwt]);

$request -> setData([
    'method' => 'GET',
    'path'   => 'v1/pais',
    'ip'     => '192.168.168.0.229',
]);

$request -> request();

if($errors = $request -> getErrors())
    exit($errors);

return $request -> getReponse();