<?php

namespace Avantis\QueryParse\LCP;

trait Getters
{
    public function getReponse()
    {
        return $this -> _response;
    }

    public function getErrors()
    {
        return $this -> _error;
    }

    public function getStatuscode()
    {
        return $this->_statuscode;
    }
}
