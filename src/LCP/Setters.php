<?php

namespace Avantis\QueryParse\LCP;

trait Setters
{

    /**
     * @param mixed $endpoint
     */
    public function setEndpoint($endpoint)
    {
        $this->_endpoint = $endpoint;
    }

    /**
     * @param mixed $method
     */
    public function setMethod($method)
    {
        $this->_method = strtoupper($method);
    }

    /**
     * @param string $error
     */

    public function setErrors($error)
    {
        $this->_error = $error;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->_data = $data;
    }

    /**
     * @param mixed $header
     */
    public function setHeader($header)
    {
        $this-> _header = $header;
    }

    /**
     * @param string $response
     */

    public function setResponse($response)
    {
        $this->_response = $response;
    }

    /**
     * @param int $status
     */

    public function setStatuscode($status)
    {
        $this->_statuscode = $status;
    }
}
