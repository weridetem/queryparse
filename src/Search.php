<?php

namespace Avantis\QueryParse;

use Avantis\QueryParse\Search\Fields;
use Avantis\QueryParse\Search\Filters;
use Avantis\QueryParse\Search\Sorts;

trait Search {

    use Fields, Filters, Sorts;

    private $_fields;
    private $_filters;
    private $_sort;

    private $_query;
    private $_regex = [
        'uuid'      => '/^\[((("equals"|"notequals"),(null|"[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}"))|(("gt"|"lt"|"gte"|"lte"),("[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}"))|(("in"),(\["[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}"(,"[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}")*\])))\]$/',
        'string'    => '/^\[(("equals",(null|"[^"\']+"))|("notequals",(null|"[^"\']+"))|("startswith","[^"\']+")|("istartswith","[^"\']+")|("endswith","[^"\']+")|("iendswith","[^"\']+")|("contains","[^"\']+")|("icontains","[^"\']+")|("in",\[("[^"\']+")(,"[^"\']+")*\]))\]$/',
        'integer'   => '/^\[(("equals",(null|(-?[0-9]+)(\.[0-9]+)?))|("notequals",(null|(-?[0-9]+)(\.[0-9]+)?))|("gt",(-?[0-9]+)(\.[0-9]+)?)|("lt",(-?[0-9]+)(\.[0-9]+)?)|("gte",(-?[0-9]+)(\.[0-9]+)?)|("lte",(-?[0-9]+)(\.[0-9]+)?)|("in",\[((-?[0-9]+)(\.[0-9]+)?)(,(-?[0-9]+)(\.[0-9]+)?)*\]))\]$/',
        'boolean'   => '/^\[("equals"|"notequals"),(null|true|false)\]$/',
        'datetime'  => '/^\[(("equals"|"notequals"|"gt"|"lt"|"gte"|"lte"),"(\d{4})-(\d{2})-(\d{2})( (\d{2}):(\d{2}):(\d{2}))?")|("equals"|"notequals"),(null)\]$/',
        'timestamp' => '/^\[(("equals"|"notequals"|"gt"|"lt"|"gte"|"lte"),"(\d{4})-(\d{2})-(\d{2})( (\d{2}):(\d{2}):(\d{2}))?")|("equals"|"notequals"),(null)\]$/',
        'time'      => '/^\[(("equals"|"notequals"|"gt"|"lt"|"gte"|"lte"),"(\d{2}):(\d{2}):(\d{2})")|("equals"|"notequals"),(null)\]$/',
    ];

    /**
     * @return array
     */
    public function getRegex()
    {
        return $this->_regex;
    }

    /**
     * @return mixed
     */
    public function getFields()
    {
        return $this->_fields;
    }

    /**
     * @return mixed
     */
    public function getFilters()
    {
        return $this->_filters;
    }

    /**
     * @return mixed
     */
    public function getSort()
    {
        return $this->_sort;
    }

    public function search(Array $params) {

        // < Input
        if(!empty($params['fields']))
            $this -> _fields = $params['fields'];

        if(!empty($params['filters']))
            $this -> _filters = $params['filters'];

        if(!empty($params['sort']))
            $this -> _sort = $params['sort'];
        // > Input

        $this -> _query = $this -> query();

        $this -> fields();  // Use file trait Fields
        $this -> filters(); // Use file trait Filters
        $this -> order();   // Use file trait Sorts

        return $this -> _query;

    }

}