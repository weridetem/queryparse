<?php

namespace Avantis\QueryParse;

use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class JWT
{

    /*
     * Extract value on token
     *
     * @param String $jwt
     * @param String $index
     * @return String
     * */
    public static function extractValue($jwt, $index) {
        if(!$jwt || !$index)
            return false;

        if (preg_match_all('/^[a-zA-Z0-9\-_]+?\.[a-zA-Z0-9\-_]+?\.([a-zA-Z0-9\-_]+)?$/', $jwt, $matches, PREG_SET_ORDER, 0)) {

            try {
                $token = (new Parser())->parse((string)$jwt);

                if (!$token->verify(new Sha256(), env('JWT_SECRET')))
                    return '';
            } catch (\RuntimeException $e) {
                return '';
            }

            $value = $token->getClaim($index);

            return $value;
        }

        return '';
    }

}