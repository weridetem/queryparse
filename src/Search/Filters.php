<?php

namespace Avantis\QueryParse\Search;

trait Filters {

    /**
     * Escapa os caracteres que por padrao o eloquent remove
     *
     * @param string $string
     * @return string
     * */

    public function escapeSpecialCharacters($string)
    {
        $search  = array('%', '_');
        $replace = array('\%', '\_');
        return str_replace($search, $replace, $string);
    }

    /**
     * Verifica se o type informado é valido e se atendende a condicao regex
     *
     * @param string $type
     * @param Array $str - Formato ['column':['condition','value']]
     * @return Boolean
     */

    private function operatorsRegex($type, $str) {

        $str   = json_encode($str);
        $regex = $this -> getRegex();

        preg_match_all($regex[$type], $str, $matches, PREG_SET_ORDER, 0);

        if(!$matches)
            return false;
        else
            return true;

    }

    /**
     * Aplica a condicao necessaria ao eloquent
     *
     * @param string $condition
     * @param string $column
     * @param any $value
     * @param boolean $or
     */

    private function applyCondition($condition, $column, $value, $or = false) {

        switch ($condition) {
            case 'equals':

                if($value === null) {
                    if ($or)
                        $this->_query->orWhere(function($q) use($column) { $q -> whereNull($column); });
                    else
                        $this->_query->whereNull($column);
                } else {
                    if ($or)
                        $this->_query->orWhere($column, '=', $value);
                    else
                        $this->_query->where($column, '=', $value);
                }

                break;

            case 'notequals':

                if($value === null) {
                    if ($or)
                        $this->_query->orWhere(function($q) use($column) { $q -> whereNotNull($column); });
                    else
                        $this->_query->whereNotNull($column);
                } else {
                    if ($or)
                        $this->_query->orWhere($column, '!=', $value);
                    else
                        $this->_query->where($column, '!=', $value);
                }

                break;

            case 'in':

                if ($or)
                    $this->_query->orWhere(function($q) use($column, $value) { $q -> whereIn($column, $value); });
                else
                    $this->_query->whereIn($column, $value);

                break;

            case 'gt':
                if ($or)
                    $this->_query->orWhere($column, '>', $value);
                else
                    $this->_query->where($column, '>', $value);

                break;

            case 'lt':
                if ($or)
                    $this->_query->orWhere($column, '<', $value);
                else
                    $this->_query->where($column, '<', $value);

                break;

            case 'gte':
                if ($or)
                    $this->_query->orWhere($column, '>=', $value);
                else
                    $this->_query->where($column, '>=', $value);

                break;

            case 'lte':
                if ($or)
                    $this->_query->orWhere($column, '<=', $value);
                else
                    $this->_query->where($column, '<=', $value);

                break;

            case 'contains':

                $value = $this -> escapeSpecialCharacters($value);

                if ($or)
                    $this->_query->orWhere($column, 'LIKE', "%".$value."%");
                else
                    $this->_query->where($column, 'LIKE', "%".$value."%");

                break;

            case 'startswith':

                $value = $this -> escapeSpecialCharacters($value);

                if ($or)
                    $this->_query->orWhere($column, 'LIKE', $value."%");
                else
                    $this->_query->where($column, 'LIKE', $value."%");

                break;

            case 'endswith':

                $value = $this -> escapeSpecialCharacters($value);

                if ($or)
                    $this->_query->orWhere($column, 'LIKE', "%".$value);
                else
                    $this->_query->where($column, 'LIKE', "%".$value);

                break;

            case 'icontains':

                $value = $this -> escapeSpecialCharacters($value);

                if ($or)
                    $this->_query->orWhere($column, 'ILIKE', "%".$value."%");
                else
                    $this->_query->where($column, 'ILIKE', "%".$value."%");

                break;

            case 'istartswith':

                $value = $this -> escapeSpecialCharacters($value);

                if ($or)
                    $this->_query->orWhere($column, 'ILIKE', $value."%");
                else
                    $this->_query->where($column, 'ILIKE', $value."%");

                break;

            case 'iendswith':

                $value = $this -> escapeSpecialCharacters($value);

                if ($or)
                    $this->_query->orWhere($column, 'ILIKE', "%".$value);
                else
                    $this->_query->where($column, 'ILIKE', "%".$value);

                break;

            case 'after':
                if ($or)
                    $this->_query->orWhere($column, '>', $value);
                else
                    $this->_query->where($column, '>', $value);

                break;

            case 'before':
                if ($or)
                    $this->_query->orWhere($column, '<', $value);
                else
                    $this->_query->where($column, '<', $value);

                break;
        }

    }

    /**
     * Recebe os parametros de filtro e os processa aplicando lacos de repeticao e condicoes de validacao de argumentos
     *
     * @return \Illuminate\Database\Eloquent\Model
     */

    private function filters()
    {

        if(!$filters = $this -> getFilters())
            return $this -> _query;

        $filters = json_decode($filters, true);

        if(!$filters)
            return $this -> _query; // nao esta no formato valido

        $or = false;

        // < Percorre AND's e OR's
        foreach ($filters as $where_clause) {

            // < Percorre as condicoes
            foreach ($where_clause as $column => list($condition, $value)) {

                // < Verifica campos obrigatorios para filtro
                if(!empty($condition)) {

                    // < Verifica se os valores informados podem ser filtrados
                    if (in_array($column, $this->fillable) && !in_array($column, $this->hidden)) {

                        $type = $this -> casts[$column];

                        // < Verifica se o campo esta preparado para o valor de entrada informado
                        if($this -> operatorsRegex($type, $where_clause[$column])) {

                            $this -> applyCondition($condition, $column, $value, $or);

                            $or = false;

                        }
                        // > Verifica se o campo esta preparado para o valor de entrada informado

                    }
                    // > Verifica se os valores informados podem ser filtrados

                }
                // > Verifica campos obrigatorios para filtro

            }
            // > Percorre as condicoes

            $or  = true;

        }
        // > Percorre AND's e OR's

        return $this -> _query;
    }

}