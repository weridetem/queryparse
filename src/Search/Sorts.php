<?php

namespace Avantis\QueryParse\Search;

trait Sorts {

    /**
     * Aplica ordenacao da consulta
     *
     * @return \Illuminate\Database\Eloquent\Model
     */

    private function order()
    {

        if(!$order = $this -> getSort())
            return $this -> _query -> orderBy($this -> primaryKey, 'ASC');

        $order = json_decode($order, true);

        if(!$order)
            return $this -> _query; // nao esta no formato valido

        $indexes = array_intersect(array_keys($order), $this -> fillable); // Permite utilizar apenas pelos valores do fillable

        if(!empty($this -> hidden))
            $indexes = array_diff($indexes, $this -> hidden); // Nao permite utilzar campos em hidden

        foreach ($indexes as $key => $index)
        {
            $value = $order[$index];

            $this -> _query -> orderBy($index, (strtoupper($value) == 'DESC') ? 'DESC' : 'ASC');
        }

        return $this -> _query;
    }

}