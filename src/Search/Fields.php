<?php

namespace Avantis\QueryParse\Search;

trait Fields {

    /**
     * Aplica quais serao os campos retornados na consulta
     *
     * @return \Illuminate\Database\Eloquent\Model
     */

    private function fields()
    {
        if(!$fields = $this -> getFields())
            return $this -> _query;

        if(str_contains($fields, ','))
        {
            $fields = explode(',', $fields);
            $fields = array_intersect($fields, $this -> fillable); // Permite utilizar apenas pelos valores do fillable
        } else {
            if(!in_array($fields, $this -> fillable))
                return $this -> _query;
        }

        return $this -> _query -> select($fields);

    }

}