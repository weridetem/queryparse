<?php

namespace Avantis\QueryParse;

use GuzzleHttp\Client;
use Avantis\QueryParse\LCP\Getters;
use Avantis\QueryParse\LCP\Setters;

/***
 * LCP = lateral communication protocol
*/

class LCP
{

    use Setters, Getters;

    private $_endpoint;
    private $_method;
    private $_data   = [];
    private $_header = [];
    private $_response;
    private $_error;
    private $_statuscode;

    private $_client;

    public function __construct($method = false, $endpoint = false, $header = false)
    {
        $this -> _method   = $method;
        $this -> _endpoint = $endpoint;
        $this -> _header   = ($header) ? $header : [];
        
        $this -> _client   = new Client;
    }

    public function request($method = false, $endpoint = false)
    {
        // < data Input
        if(!$endpoint)
            $endpoint = $this -> _endpoint;

        if(!$method)
            $method = $this -> _method;
        // > data Input

        if(!$endpoint || !$method) {
            $this->setErrors($endpoint.' and '.$method.' are required');
        } else {

            $method = strtoupper($method); // Fix

            if ($method == 'POST' && !$this->hasInHearder('content-type'))
                $this->pushHeader(['content-type' => 'application/x-www-form-urlencoded']);

            $response = $this->_client->request($method, $endpoint,
                [
                    'http_errors'   => false, // force ignor internal errors on requests
                    'headers'       => $this -> _header,
                    'form_params'   => $this -> _data,
                ]
            );

            $this->setStatuscode($response->getStatusCode());
            $this->setResponse($response->getBody());
        }
    }

    public function pushData(array $data)
    {
        $this -> setData(array_merge($data, $this -> _data));
    }

    public function pushHeader(array $data)
    {
        $this -> setHeader(array_merge($data, $this -> _header));
    }

    private function hasInHearder($index)
    {
        return !empty($this -> _header[$index]) ? true : false;
    }

}